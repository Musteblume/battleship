package program;

import game.FleetControl;

import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		int param_I = 100; //Anzahl der �quidistanten Intervalle
		int param_S = 10;  //Anzahl der Schiffe
		
		FleetControl fc = new FleetControl("141.22.26.192:8080", param_S, param_I);
		
		System.out.println("Waiting for your order General!");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Yes Sir!!!");
		fc.initialze();
		System.out.println("All systems ready for battle!");
				
		if(fc.readyForFanTheFlame()){
			System.out.println("Ready for fan the flame!");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Fire!!!");
			fc.fanTheFlame();
			
		}
		
	}
}
