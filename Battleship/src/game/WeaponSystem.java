package game;

import java.math.BigInteger;
import java.util.Random;

import chord.Chord;
import de.uniba.wiai.lspi.chord.data.ID;

public class WeaponSystem {

	private Chord chord;
	
	public WeaponSystem(Chord chord){
		this.chord = chord;
	}

	public void initilize() {
	}
	
	//Waffensysteme abfeuern:
	public void engage(){
		ID targetID = chord.ci().getID();
		ID preID = new ID( Radar.evaluate(chord.ci().getPredecessorID().toBigInteger()).toByteArray() );
		ID myID = new ID( Radar.evaluate(chord.ci().getID().toBigInteger()).toByteArray() );
		while(targetID.isInInterval(preID, myID) || targetID.compareTo(myID) == 0){	
			targetID = new ID(Radar.evaluateB(searchTarget()));
		}
		System.out.println("Fire on position " + targetID + "!");
		chord.ci().retrieve(targetID);
	}
	
	//Ziel suchen:
	private BigInteger searchTarget(){
		BigInteger n = BigInteger.valueOf(2).pow(160).subtract(BigInteger.valueOf(1));
		Random rnd = new Random();
        int maxNumBitLength = n.bitLength();
        BigInteger aRandomBigInt;
        do {
            aRandomBigInt = new BigInteger(maxNumBitLength, rnd);
            // compare random number lessthan given number
        } while (aRandomBigInt.compareTo(n) > 0); 
        return aRandomBigInt;
	}
}
