package game;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Arrays;

import chord.Chord;
import de.uniba.wiai.lspi.chord.com.Node;
import de.uniba.wiai.lspi.chord.data.ID;
import de.uniba.wiai.lspi.chord.service.NotifyCallback;


public class FleetControl implements NotifyCallback{

	private int ships;
	private int intervall;
	private Chord chord;
	private OwnFleet spForce;
	private Radar radar;
	private WeaponSystem weaponSystem;
	private boolean create = false;
	private String localAddress;
	private String bootstrapAddress;
	private boolean lastShot;
	
	private final String PATH="config/config.json";
	

	public FleetControl(String url, int ships, int intervall){	
		this.ships = ships;
		this.intervall = intervall;
		
		chord = new Chord(this);				
		
		getConfig(PATH);
		
		if(create){
			chord.create(localAddress);
		}else{
			chord.connect(localAddress, bootstrapAddress);
		}
		
		weaponSystem = new WeaponSystem(chord);		
		spForce = new OwnFleet(chord.ci().getID());
		radar = new Radar(chord, spForce, intervall, ships);
		System.out.println("FleetID: " + chord.ci().getID());
	}
	
	//Konfiguration f�r Erstellen oder Verbinden auslesen:
	private void getConfig(String path){
		try {
			InputStream fis = new FileInputStream(path);
		    InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
		    BufferedReader br = new BufferedReader(isr);
		    
		    if(br.readLine().equals("create")) {
		    	create = true; 
		    	localAddress = br.readLine();
		    	
		    }else{
		    	create = false;
		    	bootstrapAddress = br.readLine();
		    	localAddress = br.readLine();
		    }
		    
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Flottenkontrolle initialisieren:
	public void initialze(){	
		radar.initilize();
		weaponSystem.initilize();
		spForce.addShip(0);
		for(int i=1; i<ships-1; i++){
			while(!spForce.addShip((int) (Math.random() * (intervall - 0))));
		}
		spForce.addShip(intervall - 1);
		System.out.println("Fleetposition: " + spForce);
		System.out.println("Enemie Fleets:");
		for(Node fleet: chord.ci().getFingerTable()){
			System.out.println(fleet.getNodeID());
		}
	}
	
	//Das Feuer entfachen?:
	public boolean readyForFanTheFlame(){
		boolean result = false;
		byte[] max = new byte[20];
		Arrays.fill(max, (byte) 255);
		ID maxID = new ID(max);
		ID preID = new ID( Radar.evaluate(chord.ci().getPredecessorID().toBigInteger()).toByteArray() );
		ID myID = new ID( Radar.evaluate(chord.ci().getID().toBigInteger()).toByteArray() );
		result = maxID.isInInterval(preID, myID) || (maxID.compareTo(myID) == 0);
		return result;
	}
	
	//Ersten Schuss abgeben:
	public void fanTheFlame(){
		weaponSystem.engage();
	}
	
	@Override
	public void retrieved(ID target) {
		new Thread(new Runnable() {
	           public void run() {
	        	   System.out.println("Incomiiiing!");	
	       		try {
	       			Thread.sleep(500);
	       		} catch (InterruptedException e) {
	       			// TODO Auto-generated catch block
	       			e.printStackTrace();
	       		}
	       		chord.ci().broadcast(target, radar.shotHit(target));	
	       		chord.ci().setTansactionID(chord.ci().getTansactionID() + 1);
	       		try {
	       			Thread.sleep(500);
	       		} catch (InterruptedException e) {
	       			// TODO Auto-generated catch block
	       			e.printStackTrace();
	       		}
	       		weaponSystem.engage();	
	       		setLastShot(true);
		}
	}).start();		
	}

	@Override
	public void broadcast(ID source, ID target, Boolean hit) {
		System.out.println("New battleinformation Sir!");
		System.out.println("Shot on Fleet " + source + " on position " + target + "! Hit is " + hit + "!");
		radar.newBattleInformation(source, target, hit);
		System.out.println(spForce);
		if(isLastShot()){
			if(radar.checkForWin() == 1){
				System.out.println("############################################ WIN ############################################");
			setLastShot(false);
			}
		}else if(radar.checkForWin() == 1 || radar.checkForWin() == -1){
			System.out.println("############################################ LOSE ############################################");
		}	
	}
	
	public synchronized boolean isLastShot() {
		return lastShot;
	}

	public synchronized void setLastShot(boolean lastShot) {
		this.lastShot = lastShot;
	}
}
