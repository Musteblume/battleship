package game;

import java.util.HashMap;
import java.util.Map;

import de.uniba.wiai.lspi.chord.data.ID;

public class Fleet implements Comparable<Fleet>{

	private ID playernodeID;
	private Map<ID, Boolean> shotIDs;
	
	public Fleet(ID playernodeID){
		this.playernodeID = playernodeID;
		shotIDs = new HashMap<ID, Boolean>();
	}

	public ID getPlayernodeID() {
		return playernodeID;
	}

	public void setPlayernodeID(ID playernodeID) {
		this.playernodeID = playernodeID;
	}

	//Auf welche IDs der Flotte wurde schon geschossen:
	public Map<ID, Boolean> getShotIDs() {
		return shotIDs;
	}

	//Schuss auf Flotte hinzufügen:
	public void addShot(ID target, Boolean hit) {
		shotIDs.put(target, hit);
	}

	@Override
	public final int compareTo(Fleet o) {
		return this.playernodeID.compareTo(o.getPlayernodeID());
	}
		
}
