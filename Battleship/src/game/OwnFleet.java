package game;

import java.util.HashSet;
import java.util.Set;

import de.uniba.wiai.lspi.chord.data.ID;

public class OwnFleet extends Fleet {
	
	private Set<Integer> ships;
	private Set<Integer> shipsHit;
	
	public OwnFleet(ID playernodeID){
		super(playernodeID);
		ships = new HashSet<Integer>();
		shipsHit = new HashSet<Integer>();
	}
	
	//Schiff in die eigene Flotte aufnehmen:
	public boolean addShip(int ship){
		return ships.add(ship);
	}
	
	//Befindet sich ein Schiff auf der Position?:
	public boolean shipOnPosition(int position){
		return ships.contains(position);
	}
	
	//Schiff wurde getroffen:
	public void hitShip(int ship){
		shipsHit.add(ship);
	}

	@Override
	public String toString() {
		return "OwnFleet [ships=" + ships + "] - " + "[shipsHit=" + shipsHit + "]";
	}	
}
