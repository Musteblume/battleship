package game;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import chord.Chord;
import de.uniba.wiai.lspi.chord.com.Node;
import de.uniba.wiai.lspi.chord.data.ID;

public class Radar {

	private Chord chord;
	private int intervall;
	private int ships;
	private OwnFleet spForce;
	private List<Fleet> enemies;
	

	public Radar(Chord chord,OwnFleet spForce, int intervall, int ships){
		this.chord = chord;
		this.intervall = intervall;
		this.ships = ships;
		this.spForce = spForce;
		this.enemies = new ArrayList<Fleet>();
		}
	
	//Radar initialisieren:
	public void initilize(){
		for(Node fleet: chord.ci().getFingerTable()){
			enemies.add(new Fleet(fleet.getNodeID()));
		}
		enemies.add(spForce);
	}
	
	//War der eingehende Schuss ein Treffer?
	public boolean shotHit(ID target){
		boolean result = false;
		BigInteger sectorLength = calcSectorLength(chord.ci().getPredecessorID(), chord.ci().getID());
		BigInteger targetDistance = getDistance(chord.ci().getPredecessorID(), target);
		BigInteger intervallPosition = targetDistance.divide(sectorLength);
		result = spForce.shipOnPosition(intervallPosition.intValueExact());
		spForce.addShot(target, result);
		if(result) spForce.hitShip(intervallPosition.intValueExact());
		System.out.println("Hit is " + result + " on position " + target + "!");
		return result;
	}
	
	//L�nge eines Intervallsektors herausfinden:
	private BigInteger calcSectorLength(ID from, ID to){
		BigInteger fleetLength = getDistance(from , to);
		return fleetLength.divide(BigInteger.valueOf(intervall));		
	}
	
	//Entfernung zwischen zwei IDs berechnen:
	private BigInteger getDistance(ID srcID, ID dstID) {
		byte[] max = new byte[20];
		Arrays.fill(max, (byte) 255);
		ID maxID = new ID(max);
		BigInteger dist = dstID.toBigInteger().add(maxID.toBigInteger()).subtract(srcID.toBigInteger()).mod(maxID.toBigInteger());
		return evaluate(dist);		
	}
	
	//Biginteger Bytel�nge evaluieren:
	public static BigInteger evaluate(BigInteger bigint){
		byte[] tmp = new byte[20];
		
		if (bigint.toByteArray().length > 20) {
			System.arraycopy(bigint.toByteArray(), 1, tmp, 0, tmp.length);
		} else {
			tmp = bigint.toByteArray();
		}
		
		return new BigInteger(tmp);
	}
	
	//Biginteger Bytel�nge evaluieren und als Bytearray zur�ckgeben:
	public static byte[] evaluateB(BigInteger bigint){
		byte[] tmp = new byte[20];
		
		if (bigint.toByteArray().length > 20) {
			System.arraycopy(bigint.toByteArray(), 1, tmp, 0, tmp.length);
		} else {
			int j = 0;
			for(int i=0; i<20; i++){
				if(i >= 20 - bigint.toByteArray().length) tmp[i] = bigint.toByteArray()[j++];
				else tmp[i] = 0;
			}
		}
		
		return tmp;
	}
	
	//Neue Informationen �ber das Schlachtfeld speichern:
	public void newBattleInformation(ID source, ID target, Boolean hit){
		boolean enemyIsUnknown = true;
		for(Fleet f:enemies){
			if(f.getPlayernodeID().compareTo(source) == 0){
				f.addShot(target, hit);
				enemyIsUnknown = false;
				break;
			}
		}
		if(enemyIsUnknown){
			Fleet newEnemy = new Fleet(source);
			newEnemy.addShot(target, hit);
			enemies.add(newEnemy);
		}
	}

	//Hat eine Flotte gewonnen?:
	public int checkForWin() {
		int result = 0;
		enemies.sort(null);
		Iterator<Fleet> it = enemies.iterator();
		Fleet pre = null;
		while(it.hasNext()){
			Fleet f = it.next();		
			if(pre == null){
				pre = enemies.get(enemies.size()-1);			
			}
			BigInteger sectorLength = calcSectorLength(pre.getPlayernodeID(), f.getPlayernodeID());
			Set<BigInteger> hitSectors = new HashSet<BigInteger>();
			for(Map.Entry<ID, Boolean> entry: f.getShotIDs().entrySet()){
				if(entry.getValue()){
					BigInteger targetDistance = getDistance(pre.getPlayernodeID(), entry.getKey());
					hitSectors.add(targetDistance.divide(sectorLength));
				}
			}
			if(hitSectors.size() >= ships && !(f.getPlayernodeID().compareTo(chord.ci().getID()) == 0)){
				result = 1;
				break;
			}else if (hitSectors.size() >= ships){
				result = -1;
			}
			pre=f;		
		}
		return result;
	}
}
