package chord;

import game.FleetControl;

import java.net.MalformedURLException;

import de.uniba.wiai.lspi.chord.data.URL;
import de.uniba.wiai.lspi.chord.service.PropertiesLoader;
import de.uniba.wiai.lspi.chord.service.ServiceException;
import de.uniba.wiai.lspi.chord.service.impl.ChordImpl;

public class Chord {

	private ChordImpl ci;
	
	public Chord(FleetControl fc){
		PropertiesLoader.loadPropertyFile();
		
		ci = new ChordImpl();
		ci.setCallback(fc);	
	}
	
	//Chord DHT erstellen:
	public void create(String url){
		String protocol = URL.KNOWN_PROTOCOLS.get(URL.SOCKET_PROTOCOL); 
		URL localURL = null; 
		try { 
			localURL = new URL(protocol + "://" + url + "/"); 
		} catch (MalformedURLException e){ 
			throw new RuntimeException(e); 
		}
		
		try {
			ci.create(localURL);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	//Chord DHT verbinden:
	public void connect(String url, String bootstrapurl){
		String protocol = URL.KNOWN_PROTOCOLS.get(URL.SOCKET_PROTOCOL); 
		URL localURL = null; 
		try { 
			localURL = new URL(protocol + "://" + url + "/"); 
		} catch (MalformedURLException e){ 
			throw new RuntimeException(e); 
		} 
		URL bootstrapURL = null;
	 	try { 
	 		bootstrapURL = new URL(protocol + "://" + bootstrapurl + "/");
	 	} catch (MalformedURLException e){
	 		throw new RuntimeException(e);
	 	} 
		
		try {
			ci.join(localURL, bootstrapURL);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	public ChordImpl ci() {
		return ci;
	}

	public void setCi(ChordImpl ci) {
		this.ci = ci;
	}
}
